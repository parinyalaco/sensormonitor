<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('sensors', 'SensorsController');
Route::resource('sensor-parameters', 'SensorParametersController');
Route::resource('alerts', 'AlertsController');
Route::resource('groups', 'GroupsController');
Route::resource('data-logs', 'DataLogsController');

Route::get( '/apisensor/save/{sensorName}/{hum}/{tempc}/{tempf}', 'DataLogsController@saveData');
Route::get( '/apisensor/getcurrent/{sensor_id}/{type}', 'DataLogsController@getCurrent');
Route::get( '/apisensor/view/{sensor_id}/{type}', 'DataLogsController@viewguage');
Route::get( '/apisensor/getLast/{sensor_id}/{type}/{number}', 'DataLogsController@getLast');
Route::get( '/viewgraph/{sensor_id}/{type}', 'DataLogsController@viewgraph');

