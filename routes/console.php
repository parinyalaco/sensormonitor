<?php

use App\Sensor;
use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('cronrun', function () {
    $this->info("This command run console");


    $dl = new App\DataLog();
    $message = $dl->checkAlert(1);

    if(!empty($message)){
        $token = 'aaSWZjVFTwCBtWGfwTnb8SydF4JYkTSIVxbdFuey0Hv';
        $ln = new KS\Line\LineNotify($token);
        $ln->send($message);
    }  
})->describe('Display an inspiring quote');