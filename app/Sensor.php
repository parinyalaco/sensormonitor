<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sensor extends Model
{
    protected $fillable = ['name', 'desc'];

    public function sensorStatus()
    {
        return $this->hasOne('App\SensorStatus', 'sensor_id', 'id');
    }
}
