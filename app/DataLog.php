<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataLog extends Model
{
    protected $fillable = ['sensor_id','sensor_type','value'];

    public function sensor()
    {
        return $this->hasOne('App\Sensor', 'id', 'sensor_id');
    }

    public function checkAlert($id){

        $chk = Alert::where('sensor_id',$id)->get();
        $message = "";
        foreach ($chk as $key => $value) {
            $data = $this->where('sensor_id',$id)->where('sensor_type',$value->type_name)->where('value',$value->condition,$value->rate)->first();
            if(!empty($data)){
                $message = "\nAlert - Sensor : ".$data->sensor->name."(". $value->type_name.")\n";
                $message .= "Current ". $data->value. " ". $value ->condition." " .$value->rate. "\n";
            }
            
        }

        return $message;
    }
}