<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    protected $fillable = [
        'sensor_id'
        ,'type_name'
        ,'name'
        ,'rate'
        ,'condition'
        ,'start'
        ,'end'
        ,'status'
    ];

    public function sensor()
    {
        return $this->hasOne('App\Sensor', 'id', 'sensor_id');
    }

    public function sensortype()
    {
        return $this->hasOne('App\SensorParameter', 'name', 'type_name');
    }
}
