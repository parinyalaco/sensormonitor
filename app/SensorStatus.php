<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SensorStatus extends Model
{
    protected $fillable = [ 'sensor_id', 'log'];

    public function sensor()
    {
        return $this->hasOne('App\Sensor', 'id', 'sensor_id');
    }
}
