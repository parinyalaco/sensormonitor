<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SensorParameter;
use Illuminate\Http\Request;

class SensorParametersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $sensorparameters = SensorParameter::latest()->paginate($perPage);
        } else {
            $sensorparameters = SensorParameter::latest()->paginate($perPage);
        }

        return view( 'sensor-parameters.index', compact( 'sensorparameters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view( 'sensor-parameters.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        SensorParameter::create($requestData);

        return redirect( 'sensor-parameters')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $sensorparameter = SensorParameter::findOrFail($id);

        return view( 'sensor-parameters.show', compact( 'sensorparameter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $sensorparameter = SensorParameter::findOrFail($id);

        return view( 'sensor-parameters.edit', compact( 'sensorparameter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $sensorparameter = SensorParameter::findOrFail($id);
        $sensorparameter->update($requestData);

        return redirect( 'sensor-parameters')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        SensorParameter::destroy($id);

        return redirect( 'sensor-parameters')->with('flash_message', ' deleted!');
    }
}
