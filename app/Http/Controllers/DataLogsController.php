<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\DataLog;
use App\Sensor;
use App\SensorStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DataLogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $datalogs = DataLog::latest()->paginate($perPage);
        } else {
            $datalogs = DataLog::latest()->paginate($perPage);
        }

        return view( 'data-logs.index', compact( 'datalogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view( 'data-logs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        DataLog::create($requestData);

        return redirect( 'data-logs')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $datalog = DataLog::findOrFail($id);

        return view( 'data-logs.show', compact( 'datalog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $datalog = DataLog::findOrFail($id);

        return view( 'data-logs.edit', compact( 'datalog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $datalog = DataLog::findOrFail($id);
        $datalog->update($requestData);

        return redirect( 'data-logs')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        DataLog::destroy($id);

        return redirect( 'data-logs')->with('flash_message', ' deleted!');
    }

    public function saveData($sensorName,$hum,$tempc ,$tempf ){

        $sensorObj = Sensor::where('name', $sensorName)->first();

        $chkData = DataLog::where('sensor_id',$sensorObj->id)->where( 'sensor_type', 'temperature')->orderBy('created_at','desc')->first();

        if( $chkData->value <> $tempc){
            $tmpData = array();
            $tmpData['sensor_id'] = $sensorObj->id;
            $tmpData['sensor_type'] = "temperature";
            $tmpData['value'] = $tempc;
        

            DataLog::create($tmpData);
        }

        $chkData = DataLog::where('sensor_id',$sensorObj ->id)->where('sensor_type', 'humidity')->orderBy('created_at','desc')->first();

        if($chkData ->value <> $hum){

            $tmpData = array();
            $tmpData['sensor_id'] = $sensorObj->id;
            $tmpData['sensor_type'] = "humidity";
            $tmpData['value'] = $hum;

            DataLog::create($tmpData);
        }

        $sensorStatus = SensorStatus::where('sensor_id', $sensorObj->id)->first();
        if(empty($sensorStatus)){
            
            $tmpSensorStatus = array();
            $tmpSensorStatus['sensor_id'] = $sensorObj->id;
            $tmpSensorStatus['log'] = "Save Sensor : ".$sensorName. " ,humidity :  ".$hum . " ,temperature : ". $tempc;

            SensorStatus::create($tmpSensorStatus);

        }else{

            $sensorStatus->log = "Save Sensor : ".$sensorName. " ,humidity :  ".$hum . " ,temperature : ". $tempc." Date ". Carbon::now()->format('Y/m/d H:i:s');
            $sensorStatus->update();
        }

        echo "Save Complete\n";
    }

    public function getCurrent($sensor_id,$type)
    {
         $last = DataLog::where( 'sensor_id', $sensor_id)->where( 'sensor_type', $type)->orderBy('created_at','desc')->first();
        return response()->json($last);
    }

    public function viewguage($sensor_id,$type) 
    {
        $last = DataLog::where('sensor_id', $sensor_id)->where('sensor_type', $type)->orderBy('created_at','desc')->first();
        return view('graph.guage', compact('last', 'sensor_id','type'));
    }

    public function getLast($sensor_id,$type,$number)  
    {
        $last = DataLog::where('sensor_id', $sensor_id)->where('sensor_type', $type)->orderBy('created_at','desc')->paginate($number);   
        return response()->json($last);
    }

    public function viewgraph($sensor_id,$type)  {
        $last = DataLog::where('sensor_id', $sensor_id)->where('sensor_type', $type)->orderBy('created_at', 'desc')->paginate(1000);   
        
        return view( 'data-logs.graph',compact('last', 'type', 'sensor_id'));
    }
}
