<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use App\Alert;
use App\Sensor;
use App\SensorParameter;
use Illuminate\Http\Request;

class AlertsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $alerts = Alert::latest()->paginate($perPage);
        } else {
            $alerts = Alert::latest()->paginate($perPage);
        }

        return view( 'alerts.index', compact('alerts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $sensorlist = Sensor::pluck('name','id');
        $typelist = SensorParameter::pluck('name','type');
        return view('alerts.create', compact( 'sensorlist', 'typelist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $requestData['start'] = str_replace('T', ' ', $requestData['start'] );
        $requestData[ 'end'] = str_replace('T', ' ', $requestData[ 'end'] );
        $requestData['status'] = true;

        Alert::create($requestData);

        return redirect( 'alerts')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $alert = Alert::findOrFail($id);

        return view( 'alerts.show', compact( 'alert'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $alert = Alert::findOrFail($id);

        return view( 'alerts.edit', compact( 'alert'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $alert = Alert::findOrFail($id);

        $requestData['start'] = str_replace('T', ' ', $requestData['start']);
        $requestData['end'] = str_replace('T', ' ', $requestData['end']);
        $requestData['status'] = true;

        $alert->update($requestData);

        return redirect( 'alerts')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Alert::destroy($id);

        return redirect( 'alerts')->with('flash_message', ' deleted!');
    }
}
