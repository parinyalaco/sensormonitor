<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SensorParameter extends Model
{
    protected $fillable = [
        'name', 
        'type'
    ];
}
