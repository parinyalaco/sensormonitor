@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Edit Group #{{ $group->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/groups') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        

                        {!! Form::model($group, [
                            'method' => 'PATCH',
                            'url' => ['/groups', $group->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('groups.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
