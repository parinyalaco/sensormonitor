@extends('layouts.chart')

@section('content')
    <div class="container">
         <a href="{{ url('/apisensor/view/'.$sensor_id.'/'.$type) }}" title="Back"><button class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-scale" aria-hidden="true"></i> Gauage</button></a>
        <a href="{{ url('/sensors') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-triangle-left" aria-hidden="true"></i> Back</button></a>
         <div id="curve_chart" style="width: 950px; height: 500px"></div>
    </div>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Time', '{{$type}}'],
          @foreach ($last as $item)
            ['{{ $item->created_at }}',  {{ $item->value }}],
          @endforeach
          
        ]);

        var options = {
          title: '{{$type}} ย้อนหลัง',
          curveType: 'none',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
@endsection
