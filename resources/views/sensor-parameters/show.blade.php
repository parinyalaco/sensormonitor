@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">SensorParameter {{ $sensorparameter->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/sensor-parameters') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/sensor-parameters/' . $sensorparameter->id . '/edit') }}" title="Edit SensorParameter"><button class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('sensorparameters' . '/' . $sensorparameter->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete SensorParameter" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="glyphicon glyphicon-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $sensorparameter->id }}</td>
                                        <th>Name</th><td>{{ $sensorparameter->name }}</td>
                                        <th>Type</th><td>{{ $sensorparameter->type }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
