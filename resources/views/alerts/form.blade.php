<div class="form-group {{ $errors->has('sensor_id') ? 'has-error' : ''}}">
    {!! Form::label('sensor_id', 'Sensor', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('sensor_id', $sensorlist,null, ['class' => 'form-control']) !!}
        {!! $errors->first('sensor_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('type_name') ? 'has-error' : ''}}">
    {!! Form::label('type_name', 'Sensor', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('type_name', $typelist,null, ['class' => 'form-control']) !!}
        {!! $errors->first('type_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('rate') ? 'has-error' : ''}}">
    {!! Form::label('rate', 'Value', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('rate', null, ['class' => 'form-control']) !!}
        {!! $errors->first('rate', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('condition') ? 'has-error' : ''}}">
    {!! Form::label('condition', 'Condition', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('condition', null, ['class' => 'form-control']) !!}
        {!! $errors->first('condition', '<p class="help-block">:message</p>') !!}
        Code : == , != , > , < , >= , <=  
    </div>
</div>
<div class="form-group {{ $errors->has('start') ? 'has-error' : ''}}">
    {!! Form::label('start', 'Start Date', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::input('dateTime-local','start', null, ['class' => 'form-control']) !!}
        {!! $errors->first('start', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('end') ? 'has-error' : ''}}">
    {!! Form::label('end', 'End Date', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::input('dateTime-local','end', null, ['class' => 'form-control']) !!}
        {!! $errors->first('end', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
