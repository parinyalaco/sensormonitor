@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Alert {{ $alert->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/alerts') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/alerts/' . $alert->id . '/edit') }}" title="Edit Alert"><button class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-pencil " aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('alerts' . '/' . $alert->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Alert" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="glyphicon glyphicon-trash " aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $alert->id }}</td>
                                        <th>Sensor</th><td>{{ $alert->sensor->name }}</td>
                                        <th>Sensor</th><td>{{ $alert->sensorType->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Name</th><td>{{ $alert->name }}</td>
                                        <th>condition</th><td>{{ $alert->condition }}</td>
                                        <th>value</th><td>{{ $alert->rate }}</td>
                                    </tr>
                                    <tr>
                                        <th>Start</th><td>{{ $alert->start }}</td>
                                        <th>End</th><td>{{ $alert->end }}</td>
                                        <th>Status</th><td>{{ $alert->status }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
