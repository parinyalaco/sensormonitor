@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Edit Sensor #{{ $sensor->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/sensors') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($sensor, [
                            'method' => 'PATCH',
                            'url' => ['/sensors', $sensor->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('sensors.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}



            

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
