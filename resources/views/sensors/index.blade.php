@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Sensors</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Last Update</th>
                                        <th>View</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($sensors as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->sensorStatus->updated_at }}</td>
                                        <td>
                                            <a href="{{ url('/apisensor/view/' . $item->id. "/temperature") }}" title="View temperature"><button class="btn btn-info btn-sm"><i class="glyphicon glyphicon-fire" aria-hidden="true"></i> temperature</button></a>
                                            <a href="{{ url('/apisensor/view/' . $item->id. "/humidity") }}" title="View humidity"><button class="btn btn-info btn-sm"><i class="glyphicon glyphicon-tint" aria-hidden="true"></i> humidity</button></a>
                                            
                                           
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $sensors->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
