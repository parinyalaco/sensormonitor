<html>
  <head>
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   <script type="text/javascript">
      google.charts.load('current', {'packages':['gauge']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['{{ $last->sensor_type }}', 0]
        ]);

        var options = {
          width: 700, height: 300,
          redFrom: 90, redTo: 100,
          yellowFrom:75, yellowTo: 90,
          minorTicks: 20
        };

        var chart = new google.visualization.Gauge(document.getElementById('chart_div'));

        chart.draw(data, options);

        setInterval(function() {
          $.getJSON(
            '/wh_sensor/apisensor/getcurrent/{{ $last->sensor_id }}/{{ $last->sensor_type }}',
            function(mydata) {
              data.setValue(0, 1, mydata['value']);
              chart.draw(data, options);
            }
          );

          $.getJSON(
            '/wh_sensor/apisensor/getLast/{{ $last->sensor_id }}/{{ $last->sensor_type }}/10',
            function(mydata) {
              tmp = "";
              $.each(mydata['data'], function(key,value){
                tmp = tmp + " เวลา : " + value['created_at'];
                tmp = tmp + " | {{ $last->sensor_type }} : " + value['value'] + " <br/>";
              });
              $('#table_div').html(tmp);
            }
          );

          
        }, 5000);
      }
    </script>
  </head>
  <body>
  <h1>ระบบวัดค่า {{ $last->sensor->name }} {{ $last->sensor_type }} </h1>
  <a href="{{ url('/viewgraph/'.$sensor_id.'/'.$type) }}" title="Back"><button class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-scale" aria-hidden="true"></i> Graph</button></a>
    <a href="{{ url('/sensors') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-back" aria-hidden="true"></i> Back</button></a>
  
  <div id="chart_div" style="width: 700px; height: 300px;"></div><br/>

    <div id="table_div"></div>
  </body>
</html>